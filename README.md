# Gitlab-CI & Condor: Mandelbrot Tutorial

A Mandelbrot program generator using C with the gcc compiler.

## Running locally
After you have navigated to your directory in a local shell, you should begin by compiling and running the mandelbrot first. Considering we are using a gcc compiler in C, things should work without any problems here. Run:
```
gcc mandelbrot.c
```
```
./a.out
```
Now open the newly generated/regenerated mandelbrot.ppm file using

```
open mandelbrot.ppm
```

## Gitlab-CI on a Local Machine

It is a good idea to learn the basics of Gitlab CI by installing a runner on a local machine first. In this way, you will have more control over your runner abilities thru the Gitlab API, and more notably any permissions issues your runner can sometimes run often into.

Cool. So now go and install a [binary runner on your machine.](https://docs.gitlab.com/runner/install/#binaries)

We will try to cover the basic syntax for extending and running a job with gitlab-ci, but after downloading this repo, you can jump into the YAML syntax for [configuring your jobs](https://docs.gitlab.com/ee/ci/yaml/)

I will also cover the basics on pipelines, but you can start reading more [in-depth stuff on this here](https://docs.gitlab.com/ee/ci/pipelines.html)

You can see some other [example projects](https://docs.gitlab.com/ee/ci/examples/README.html) that utilise gitlab-ci in various languages example projects.

## Gitlab-CI in the Condor Pool

In part II of this series in January, we will attempt to use a Gitlab-CI as a means to submitting jobs with HTCondor through ```condor_submit```
